package com.example.gelso.activityresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.w3c.dom.Text;

public class Main2Activity extends AppCompatActivity {

    Button btn_save;
    Button btn_cancel;
    EditText txt_age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        txt_age = (EditText) findViewById(R.id.txt_age);

        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent i = new Intent(getApplicationContext(), MainActivity.class);
                //i.putExtra("age", txt_age.getText().toString());
                //startActivity(i);
                //finish();

                Intent i = getIntent();
                i.putExtra("age", txt_age.getText().toString());
                setResult(RESULT_OK,i);
                finish();

            }
        });


        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent i = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(i);
                //finish();

                Intent i = getIntent();
                setResult(RESULT_CANCELED,i);
                finish();

            }
        });


    }
}


